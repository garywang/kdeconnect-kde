# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeconnect-kde package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeconnect-kde\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-12 00:45+0000\n"
"PO-Revision-Date: 2022-08-24 11:26+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Sveinn í Felli"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sv1@fellsnet.is"

#: kcm.cpp:37
#, kde-format
msgid "KDE Connect Settings"
msgstr "Stillingar KDE Connect"

#: kcm.cpp:39
#, kde-format
msgid "KDE Connect Settings module"
msgstr "Stillingareining KDE Connect"

#: kcm.cpp:41
#, kde-format
msgid "(C) 2015 Albert Vaca Cintora"
msgstr "(C) 2015 Albert Vaca Cintora"

#: kcm.cpp:44
#, kde-format
msgid "Albert Vaca Cintora"
msgstr "Albert Vaca Cintora"

#: kcm.cpp:223
#, kde-format
msgid "Key: %1"
msgstr "Lykill: %1"

#: kcm.cpp:254
#, kde-format
msgid "Available plugins"
msgstr "Tiltækar viðbætur"

#: kcm.cpp:307
#, kde-format
msgid "Error trying to pair: %1"
msgstr "Villa þegar reynt var að para: %1"

#: kcm.cpp:328
#, kde-format
msgid "(paired)"
msgstr "(parað)"

#: kcm.cpp:331
#, kde-format
msgid "(not paired)"
msgstr "(ekki parað)"

#: kcm.cpp:334
#, kde-format
msgid "(incoming pair request)"
msgstr "(móttekin beiðni um pörun)"

#: kcm.cpp:337
#, kde-format
msgid "(pairing requested)"
msgstr "(beðið um pörun)"

#. i18n: ectx: property (text), widget (QLabel, rename_label)
#: kcm.ui:59
#, kde-format
msgid "KDE Connect"
msgstr "KDE Connect"

#. i18n: ectx: property (text), widget (QToolButton, renameShow_button)
#: kcm.ui:82
#, kde-format
msgid "Edit"
msgstr "Breyta"

#. i18n: ectx: property (text), widget (QToolButton, renameDone_button)
#: kcm.ui:104
#, kde-format
msgid "Save"
msgstr "Vista"

#. i18n: ectx: property (text), widget (QPushButton, refresh_button)
#: kcm.ui:120
#, kde-format
msgid "Refresh"
msgstr "Endurlesa"

#. i18n: ectx: property (text), widget (QLabel, name_label)
#: kcm.ui:193
#, kde-format
msgid "Device"
msgstr "Tæki"

#. i18n: ectx: property (text), widget (QLabel, status_label)
#: kcm.ui:209
#, kde-format
msgid "(status)"
msgstr "(staða)"

#. i18n: ectx: property (text), widget (KSqueezedTextLabel, verificationKey)
#: kcm.ui:232
#, kde-format
msgid "🔑 abababab"
msgstr "🔑 abababab"

#. i18n: ectx: property (text), widget (QPushButton, accept_button)
#: kcm.ui:276
#, kde-format
msgid "Accept"
msgstr "Samþykkja"

#. i18n: ectx: property (text), widget (QPushButton, reject_button)
#: kcm.ui:283
#, kde-format
msgid "Reject"
msgstr "Hafna"

#. i18n: ectx: property (text), widget (QPushButton, pair_button)
#: kcm.ui:296
#, kde-format
msgid "Request pair"
msgstr "Biðja um pörun"

#. i18n: ectx: property (text), widget (QPushButton, unpair_button)
#: kcm.ui:309
#, kde-format
msgid "Unpair"
msgstr "Afpara"

#. i18n: ectx: property (text), widget (QPushButton, ping_button)
#: kcm.ui:322
#, kde-format
msgid "Send ping"
msgstr "Senda ping-beiðni "

#. i18n: ectx: property (text), widget (QLabel, noDeviceLinks)
#: kcm.ui:360
#, kde-format
msgid ""
"<html><head/><body><p>No device selected.<br><br>If you own an Android "
"device, make sure to install the <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE Connect Android app</span></a> (also "
"available <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
"kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">from F-Droid</span></a>) and it should appear in the list.<br><br>If you "
"are having problems, visit the <a href=\"https://userbase.kde.org/KDEConnect"
"\"><span style=\" text-decoration: underline; color:#4c6b8a;\">KDE Connect "
"Community wiki</span></a> for help.</p></body></html>"
msgstr ""
"<html><head/><body><p>Ekkert tæki er valið.<br><br>Ef þú átt Android tæki, "
"gakktu úr skugga um að setja upp <a href=\"https://play.google.com/store/"
"apps/details?id=org.kde.kdeconnect_tp\"><span style=\" text-decoration: "
"underline; color:#4c6b8a;\">KDE Connect Android forritið</span></a> (einnig "
"tiltækt <a href=\"https://f-droid.org/repository/browse/?fdid=org.kde."
"kdeconnect_tp\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">frá F-Droid</span></a>) og þá ætti það að birtast á listanum.<br><br>Ef "
"þú átt í vandræðum, geturðu skoðað <a href=\"https://userbase.kde.org/"
"KDEConnect\"><span style=\" text-decoration: underline; color:#4c6b8a;"
"\">wiki-síður KDE Connect samfélagsins</span></a>.</p></body></html>"
